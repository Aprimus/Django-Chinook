from django.test import LiveServerTestCase

from chinook.models import Album, Artist

from selenium import webdriver
from selenium.webdriver.common.keys import Keys


class CommonSetupTestData(LiveServerTestCase):
	def setUp(self):
		self.browser = webdriver.Firefox()
		self.browser.implicitly_wait(2)
		self.artist_1 = Artist.objects.create(name='Eric Clapton')
		self.album_1 = Album.objects.create(title='Blues', artist=self.artist_1)

		self.artist_2 = Artist.objects.create(name='Son House')
		self.album_2 = Album.objects.create(title='Delta Blues', artist=self.artist_2)
		self.album_3 = Album.objects.create(title='Live', artist=self.artist_1)

	def tearDown(self):
		self.browser.quit()

	def check_if_table_header_contains(self, match, elements):
		self.assertEqual(match, [element.text for element in elements])

	def check_if_table_row_contains(self, match, elements):
		first_entry_data = elements.find_elements_by_tag_name('td')
		self.assertEqual(match, [entry.text for entry in first_entry_data])


class NewVisitorAlbumsTest(CommonSetupTestData):

	def check_if_elements_in_table(self, text):
		table = self.browser.find_element_by_id('elements_table')
		rows = table.find_elements_by_tag_name('tr')
		self.assertIn(text, [row.text for row in rows])

	def test_user_visit_albums_page(self):
		# User visit home page
		self.browser.get(self.live_server_url + '/')

		# User notices the page title and header mention Chinook
		self.assertIn('Chinook', self.browser.title)

		# Ands a series of links on a table

		title_links = ['Albums', 'Artists', 'Genres', 'Persons']

		[self.check_if_elements_in_table(item) for item in title_links]

		# User finds albums links on main page
		albums_link = self.browser.find_element_by_id('id_album_link')

		# and click on it

		albums_link.click()

		self.assertRegex(self.browser.current_url, '/albums/')

		# Now user is on the albums list page
		self.assertEqual(self.browser.current_url, self.live_server_url + '/albums/')

		# with Album Header

		self.assertIn('Albums', self.browser.find_element_by_tag_name('body').text)

		# And a list of albums in a table

		table = self.browser.find_element_by_id('id_albums_list')

		# The header columns in the table

		header_colums = table.find_elements_by_css_selector('th')

		# contains
		self.check_if_table_header_contains(['Title', 'Artist'], header_colums)

		# The first album in Album Table

		first_album_entry = table.find_element_by_id('album_1')

		# Contains

		self.check_if_table_row_contains(['Blues', 'Eric Clapton'], first_album_entry)

		# The second album in Album Table

		second_album_entry = table.find_element_by_id('album_2')

		# Contains

		self.check_if_table_row_contains(['Delta Blues', 'Son House'], second_album_entry)

		# User sees too many results in albums.html

		# And submit a search by album title

		search_album = self.browser.find_element_by_id('search_albums')
		placeholder = search_album.get_attribute('placeholder')
		self.assertEqual(placeholder, 'search album')

		# for all albums that contain the word blues

		search_album.send_keys('blues')
		submit_button = self.browser.find_element_by_id('search_button')
		submit_button.click()
		self.assertEqual(self.browser.current_url, self.live_server_url + '/albums/?q=blues')

		filtered_table = self.browser.find_element_by_id('id_albums_list')

		filtered_table_results = filtered_table.find_elements_by_id('album_title')

		# and get only two results instead of all albums list

		self.assertEqual(len([result.text for result in filtered_table_results]), 2)

		# User return to home page

		self.browser.find_element_by_id('home_page').click()

		self.assertEqual(self.browser.current_url, self.live_server_url + '/')


class NewVisitorArtistsTest(CommonSetupTestData):

	def test_user_visit_artist_page(self):
		# User is on main page
		self.browser.get(self.live_server_url + '/')

		# User click on artist link
		self.browser.find_element_by_id('id_artist_link').click()

		# User is now on artists page
		self.assertEqual(self.browser.current_url, self.live_server_url + '/artists/')

		# User sees the header of the page
		self.assertIn('Artists', self.browser.find_element_by_tag_name('body').text)

		# User sees the artist list table
		table = self.browser.find_element_by_id('id_artists_list')

		# The header columns in the table

		header_colums = table.find_elements_by_css_selector('th')

		# contains
		self.check_if_table_header_contains(['Name'], header_colums)

		# the table contains
		first_artist_entry = table.find_element_by_id('artist_1')
		second_artist_entry = table.find_element_by_id('artist_2')

		self.check_if_table_row_contains(['Eric Clapton'], first_artist_entry)
		self.check_if_table_row_contains(['Son House'], second_artist_entry)

		# User sees too many results in artist.html

		# And submit a search by artist name

		search_artist = self.browser.find_element_by_id('search_artist')
		placeholder = search_artist.get_attribute('placeholder')
		self.assertEqual(placeholder, 'search artist')

		# for all artist that contain the word clapton

		search_artist.send_keys('clapton')
		submit_button = self.browser.find_element_by_id('search_button')
		submit_button.click()
		self.assertEqual(self.browser.current_url, self.live_server_url + '/artists/?q=clapton')

		filtered_table = self.browser.find_element_by_id('id_artists_list')

		filtered_table_results = filtered_table.find_elements_by_id('artist_name')

		# and get only one results instead of all albums list

		self.assertEqual(len([result.text for result in filtered_table_results]), 1)

		# User return to home page

		self.browser.find_element_by_id('home_page').click()

		self.assertEqual(self.browser.current_url, self.live_server_url + '/')

		self.fail('Finish Tests')