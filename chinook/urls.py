from django.conf.urls import url

from . import views

urlpatterns = [
    url(
        regex=r'^$',
        view=views.Index.as_view(),
        name='index'
    ),

    url(
        regex=r'^albums/$',
        view=views.AlbumListView.as_view(),
        name='album_list'
    ),

    url(
        regex=r'^artists/$',
        view=views.ArtistListView.as_view(),
        name='artist_list'
    ),
]