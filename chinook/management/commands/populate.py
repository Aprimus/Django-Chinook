import sqlite3

from django.core.management.base import BaseCommand
from django.conf import settings

from chinook.models import Album, Artist, Customer, Employee, Genre, Invoice, InvoiceLine, \
    MediaType, PlayList, PlayListTrack, Track

CHINOOK_DB_PATH = settings.CHINOOK_DB


class ChinookPopulate:
    CHINOOK_DB_PATH = settings.CHINOOK_DB
    _raw_sql = {
        'album': "SELECT * FROM Album",
        'artist': "SELECT * FROM Artist",
        'customer': "SELECT * FROM Customer",
        'employee': "SELECT * FROM Employee",
        'Genre': "SELECT * FROM Genre",
        'Invoice': "SELECT * FROM Invoice",
        'InvoiceLine': "SELECT * FROM InvoiceLine",
        'MediaType': "SELECT * FROM MediaType",
        'PlayList': "SELECT * FROM Playlist",
        'PlayListTrack': "SELECT * FROM PlaylistTrack",
        'Track': "SELECT * FROM Track",
    }

    def __init__(self):
        self.conn = sqlite3.connect(self.CHINOOK_DB_PATH)
        self.c = self.conn.cursor()
        self.insert_objects_list = []

    def clean(self):
        self.insert_objects_list.clear()

    def populate_album(self):
        d = self.c.execute(self._raw_sql['album'])
        for album in d:
            a = Album(id=album[0], title=album[1], artist_id=album[2])
            self.insert_objects_list.append(a)
        Album.objects.bulk_create(self.insert_objects_list)
        self.clean()

    def populate_artist(self):
        d = self.c.execute(self._raw_sql['artist'])
        for artist in d:
            a = Artist(id=artist[0], name=artist[1])
            self.insert_objects_list.append(a)
        Artist.objects.bulk_create(self.insert_objects_list)
        self.clean()

    def populate_customer(self):
        d = self.c.execute(self._raw_sql['customer'])
        for customer in d:
            c = Customer(
                id=customer[0],
                first_name=customer[1],
                last_name=customer[2],
                company=customer[3],
                address=customer[4],
                city=customer[5],
                state=customer[6],
                country=customer[7],
                postal_code=customer[8],
                phone=customer[9],
                fax=customer[10],
                email=customer[11],
                support_rep_id=customer[12]
            )
            self.insert_objects_list.append(c)
        Customer.objects.bulk_create(self.insert_objects_list)
        self.clean()

    def populate_employee(self):
        d = self.c.execute(self._raw_sql['employee'])
        for empl in d:
            e = Employee(
                    id=empl[0],
                    last_name=empl[1],
                    first_name=empl[2],
                    title=empl[3],
                    reports_to_id=empl[4],
                    birth_date=empl[5],
                    hire_date=empl[6],
                    address=empl[7],
                    city=empl[8],
                    state=empl[9],
                    country=empl[10],
                    postal_code=empl[11],
                    phone=empl[12],
                    fax=empl[13],
                    email=empl[14]
            )
            self.insert_objects_list.append(e)
        Employee.objects.bulk_create(self.insert_objects_list)
        self.clean()

    def populate_genre(self):
        d = self.c.execute(self._raw_sql['Genre'])
        for genre in d:
            g = Genre(id=genre[0], name=genre[1])
            self.insert_objects_list.append(g)
        Genre.objects.bulk_create(self.insert_objects_list)
        self.clean()

    def populate_invoice(self):
        d = self.c.execute(self._raw_sql['Invoice'])
        for invoice in d:
            i = Invoice(
                id=invoice[0],
                customer_id=invoice[1],
                invoice_date=invoice[2],
                billing_address=invoice[3],
                billing_city=invoice[4],
                billing_state=invoice[5],
                billing_country=invoice[6],
                billing_postal_code=invoice[7],
                total=invoice[8]
            )
            self.insert_objects_list.append(i)
        Invoice.objects.bulk_create(self.insert_objects_list)
        self.clean()

    def populate_invoice_line(self):
        d = self.c.execute(self._raw_sql['InvoiceLine'])
        for invline in d:
            i = InvoiceLine(
                id=invline[0],
                invoice_id=invline[1],
                track_id=invline[2],
                unit_price=invline[3],
                quantity=invline[4]
            )
            self.insert_objects_list.append(i)
        InvoiceLine.objects.bulk_create(self.insert_objects_list)
        self.clean()

    def populate_media_type(self):
        d = self.c.execute(self._raw_sql['MediaType'])
        for media_type in d:
            m = MediaType(id=media_type[0], name=media_type[1])
            self.insert_objects_list.append(m)
        MediaType.objects.bulk_create(self.insert_objects_list)
        self.clean()

    def populate_play_list(self):
        d = self.c.execute(self._raw_sql['PlayList'])
        for play_list in d:
            p = PlayList(id=play_list[0], name=play_list[1])
            self.insert_objects_list.append(p)
        PlayList.objects.bulk_create(self.insert_objects_list)
        self.clean()

    def populate_play_list_track(self):
        d = self.c.execute(self._raw_sql['PlayListTrack'])
        for play_list_track in d:
            p = PlayListTrack(play_list_id=play_list_track[0], track_id=play_list_track[1])
            self.insert_objects_list.append(p)
        PlayListTrack.objects.bulk_create(self.insert_objects_list)
        self.clean()

    def populate_track(self):
        d = self.c.execute(self._raw_sql['Track'])
        for track in d:
            t = Track(
                id=track[0],
                name=track[1],
                album_id=track[2],
                media_type_id=track[3],
                genre_id=track[4],
                composer=track[5],
                millisecond=track[6],
                bytes=track[7],
                unit_price=track[8]
            )
            self.insert_objects_list.append(t)
        Track.objects.bulk_create(self.insert_objects_list)
        self.clean()


class Command(BaseCommand):
    help = 'Populate the chinook database'

    def handle(self, *args, **options):
        c = ChinookPopulate()
        c.populate_album()
        c.populate_artist()
        c.populate_customer()
        c.populate_employee()
        c.populate_genre()
        c.populate_invoice()
        c.populate_invoice_line()
        c.populate_media_type()
        c.populate_play_list()
        c.populate_play_list_track()
        c.populate_track()
