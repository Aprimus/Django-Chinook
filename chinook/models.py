from django.db import models


class Album(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=200)
    artist = models.ForeignKey('Artist')

    def __str__(self):
        return self.title


class MixinName(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        abstract = True


class Artist(MixinName):
    pass


class Genre(MixinName):
    pass


class Person(models.Model):
    id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    address = models.CharField(max_length=100, blank=True, null=True)
    city = models.CharField(max_length=50, blank=True, null=True)
    state = models.CharField(max_length=50, blank=True, null=True)
    country = models.CharField(max_length=50, blank=True, null=True)
    postal_code = models.CharField(max_length=10, blank=True, null=True)
    phone = models.CharField(max_length=50, blank=True, null=True)
    fax = models.CharField(max_length=50, blank=True, null=True)
    email = models.EmailField(max_length=60)

    class Meta:
        abstract = True

    def __str__(self):
        return '{0}-{1}'.format(self.first_name, self.last_name)


class Customer(Person):
    company = models.CharField(max_length=100, blank=True, null=True)
    support_rep = models.ForeignKey('Employee', null=True, blank=True)


class Employee(Person):
    title = models.CharField(max_length=30, blank=True, null=True)
    reports_to = models.ForeignKey("Employee", null=True, blank=True)
    birth_date = models.DateTimeField(null=True, blank=True)
    hire_date = models.DateTimeField(null=True, blank=True)


class Invoice(models.Model):
    id = models.AutoField(primary_key=True)
    customer = models.ForeignKey(Customer)
    invoice_date = models.DateTimeField()
    billing_address = models.CharField(max_length=70, blank=True, null=True)
    billing_city = models.CharField(max_length=40, blank=True, null=True)
    billing_state = models.CharField(max_length=40, blank=True, null=True)
    billing_country = models.CharField(max_length=40, blank=True, null=True)
    billing_postal_code = models.CharField(max_length=10, blank=True, null=True)
    total = models.PositiveIntegerField(blank=True, null=True)

    def __str__(self):
        return '{} - {}'.format(self.id, self.customer)


class InvoiceLine(models.Model):
    id = models.AutoField(primary_key=True)
    invoice = models.ForeignKey(Invoice)
    track = models.ForeignKey('Track')
    unit_price = models.DecimalField(max_digits=5, decimal_places=2)
    quantity = models.IntegerField()

    def __str__(self):
        return '{}-{}'.format(self.invoice, self.track)


class MediaType(MixinName):
    pass


class PlayList(MixinName):
    pass


class PlayListTrack(models.Model):
    play_list = models.ForeignKey(PlayList)
    track = models.ForeignKey('Track')

    class Meta:
        unique_together = ('play_list', 'track')

    def __str__(self):
        return '{} - {}'.format(self.play_list, self.track)


class Track(MixinName):
    play_list = models.ManyToManyField(PlayList, through=PlayListTrack, blank=True)
    album = models.ForeignKey(Album, null=True, blank=True)
    media_type = models.ForeignKey(MediaType)
    genre = models.ForeignKey(Genre, blank=True, null=True)
    composer = models.CharField(max_length=220, blank=True, null=True)
    millisecond = models.IntegerField()
    bytes = models.IntegerField(blank=True, null=True)
    unit_price = models.DecimalField(max_digits=5, decimal_places=2)
