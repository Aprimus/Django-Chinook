from django.test import TestCase
from django.core.urlresolvers import resolve

from chinook.views import Index, AlbumListView, ArtistListView


class ChinookUrlTestCase(TestCase):
	def test_home_page_use_correct_view(self):
		root = resolve('/')
		self.assertEqual(root.func.view_class, Index)

	def test_albums_page_use_correct_view(self):
		albums_url = resolve('/albums/')
		self.assertEqual(albums_url.func.view_class, AlbumListView)

	def test_artist_page_use_correct_view(self):
		artists_url = resolve('/artists/')
		self.assertEqual(artists_url.func.view_class, ArtistListView)