from datetime import datetime

from django.test import TestCase
from django.utils import timezone
from django.db import IntegrityError

from ..models import Artist, Album, Genre, Employee, \
	Customer, Invoice, InvoiceLine, Track, PlayList, \
	PlayListTrack, MediaType


class CommonModelsTests(TestCase):

	def setUp(self):
		self.artist = Artist.objects.create(name='Artist1')
		self.album = Album.objects.create(title='Title1', artist=self.artist)
		self.genre = Genre.objects.create(name='Genre1')

		self.employee = Employee.objects.create(
			first_name='John',
			last_name='Wick',
			address='Harrisville Street',
			city='San Fantastico',
			state='YokaiLand',
			country='Japan',
			postal_code='1234',
			phone='0120304',
			fax='987654',
			email='john@wick.com',
			title='Doctor',
			birth_date=datetime(year=1978, month=2, day=28, tzinfo=timezone.utc),
			hire_date=datetime(year=1999, month=1, day=30, tzinfo=timezone.utc)
		)

		self.customer = Customer.objects.create(
			first_name='Neith',
			last_name='Richards',
			address='Downtown Springdale Street',
			city='Springdale',
			state='WildWood',
			country='Spring',
			postal_code='1234',
			phone='0120304',
			fax='987654',
			email='neith@richards.com',
			company='Sega llc',
			support_rep=self.employee
		)

		self.invoice = Invoice.objects.create(
			customer=self.customer,
			invoice_date=datetime(year=2018, month=2, day=15, tzinfo=timezone.utc),
			billing_address='address1',
			billing_city='city1',
			billing_state='state1',
			billing_country='country1',
			billing_postal_code='postal1',
			total=3
		)

		self.play_list = PlayList.objects.create(
			name='Playlist1'
		)

		self.media_type = MediaType.objects.create(
			name='Mediatype1'
		)

		self.track = Track.objects.create(
			name='trackname1',
			album=self.album,
			media_type=self.media_type,
			genre=self.genre,
			composer='composer1',
			millisecond=2,
			bytes=45,
			unit_price='12.978'
		)

		self.play_list_track = PlayListTrack.objects.create(
			play_list=self.play_list,
			track=self.track
		)

	def test_objects_creation(self):
		self.assertEqual(Album.objects.count(), 1)
		self.assertEqual(Artist.objects.count(), 1)
		self.assertEqual(Genre.objects.count(), 1)

		self.assertEqual(Employee.objects.count(), 1)
		self.assertEqual(Customer.objects.count(), 1)

		self.assertEqual(Invoice.objects.count(), 1)
		self.assertEqual(PlayList.objects.count(), 1)
		self.assertEqual(MediaType.objects.count(), 1)
		self.assertEqual(Track.objects.count(), 1)
		self.assertEqual(PlayListTrack.objects.count(), 1)

	def test_foreign_key_model(self):
		self.assertEqual(self.track.album, self.album)
		self.assertIn(self.track, self.album.track_set.all())

	def test_m2m_trough_model(self):
		self.assertEqual(
			self.track.play_list.first(), self.play_list
		)

	def test_constrain(self):
		with self.assertRaises(IntegrityError):
			PlayListTrack.objects.create(play_list=self.play_list, track=self.track)

	def test_string_representation(self):
		self.assertEqual(str(self.album), 'Title1')
		self.assertEqual(str(self.artist), 'Artist1')
		self.assertEqual(str(self.genre), 'Genre1')
		self.assertEqual(str(self.customer), 'Neith-Richards')
		self.assertEqual(str(self.employee), 'John-Wick')
		self.assertEqual(str(self.invoice), '1 - Neith-Richards')
		self.assertEqual(str(self.play_list), 'Playlist1')
		self.assertEqual(str(self.media_type), 'Mediatype1')
		self.assertEqual(str(self.track), 'trackname1')

