from django.test import TestCase, RequestFactory

from ..views import Index, AlbumListView, ArtistListView
from ..models import Album, Artist


class BaseTestCase(TestCase):
	def setUp(self):
		self.factory = RequestFactory()
		self.artist_1 = Artist.objects.create(name='Eric Clapton')
		self.album_1 = Album.objects.create(title='Blues', artist=self.artist_1)

		self.artist_2 = Artist.objects.create(name='Son House')
		self.album_2 = Album.objects.create(title='Delta Blues', artist=self.artist_2)
		self.album_3 = Album.objects.create(title='Live', artist=self.artist_1)


class IndexViewTestCase(BaseTestCase):

	def test_basic(self):
		request = self.factory.get('/')
		response = Index.as_view()(request)
		self.assertEqual(response.status_code, 200)
		with self.assertTemplateUsed('home.html'):
			response.render()


class AlbumListViewTestCase(BaseTestCase):

	def test_basic(self):
		request = self.factory.get('/albums/')
		response = AlbumListView.as_view()(request)
		self.assertEqual(len(response.context_data['album_list']), 3)
		with self.assertTemplateUsed('albums.html'):
			response.render()

	def test_view_with_filter(self):
		request = self.factory.get('/albums/', {'q': 'Blues'})
		response = AlbumListView.as_view()(request)
		self.assertEqual(len(response.context_data['album_list']), 2)


class ArtistListViewTestCase(BaseTestCase):

	def test_basic(self):
		request = self.factory.get('/artists/')
		response = ArtistListView.as_view()(request)
		self.assertEqual(response.status_code, 200)
		self.assertEqual(len(response.context_data['artist_list']), 2)
		with self.assertTemplateUsed('artists.html'):
			response.render()

	def test_view_with_filter(self):
		request = self.factory.get('/artists/', {'q': 'clapton'})
		response = ArtistListView.as_view()(request)
		self.assertEqual(len(response.context_data['artist_list']), 1)
