from django.contrib import admin

from . import models

admin.site.register(models.Album)
admin.site.register(models.Artist)
admin.site.register(models.Customer)
admin.site.register(models.Employee)
admin.site.register(models.Genre)
admin.site.register(models.Invoice)
admin.site.register(models.InvoiceLine)
admin.site.register(models.MediaType)
admin.site.register(models.PlayList)
admin.site.register(models.Track)








