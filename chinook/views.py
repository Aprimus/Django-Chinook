from django.shortcuts import render_to_response, render
from django.views.generic import ListView, TemplateView


from .models import Album, Artist


class Index(TemplateView):
    template_name = 'home.html'


class AlbumListView(ListView):
    model = Album
    template_name = 'albums.html'

    def get_queryset(self):
        queryset = super(AlbumListView, self).get_queryset()
        q = self.request.GET.get('q')
        if q:
            return queryset.filter(title__icontains=q)
        return queryset


class ArtistListView(ListView):
    model = Artist
    template_name = 'artists.html'

    def get_queryset(self):
        queryset = super(ArtistListView, self).get_queryset()
        q = self.request.GET.get('q')
        if q:
            return queryset.filter(name__icontains=q)
        return queryset
